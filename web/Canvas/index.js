/**style ==> image ==> scale ==> [1,-1] 当为负数时表示翻转 */
const _styles = {
    "Station": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/station.png',
            scale: 0.4
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -18
        })
    }),
    "StationOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/station_offline.png',
            scale: 0.4
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -18
        })
    }),
    "StationGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#05bdb2'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Telphone": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/telphone.png',
            scale: 1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "TelphoneOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/telphone_offline.png',
            scale: 0.5
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "TelphoneGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#817cfa'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Camera": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/camera.png',
            scale: 0.5
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "CameraOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/camera_offline.png',
            scale: 0.5
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "CameraGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#6cbc97'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "BroadcastGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#ff9f17'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Broadcast": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast.png',
            scale: 1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "BroadcastOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_offline.png',
            scale: 0.5
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "BroadcastA": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_A.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BroadcastAOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_A_offline.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BroadcastB": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_B.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BroadcastBOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_B_offline.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BroadcastC": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_C.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BroadcastCOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_C_offline.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BroadcastD": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_D.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BroadcastDOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/broadcast_D_offline.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "CarGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#7993ba'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Car": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "CarRed": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car003": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car003.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car003Red": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car003-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car004": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car004.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car004Red": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car004-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car005": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car005.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car005Red": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car005-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car006": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car006.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car006Red": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car006-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car008": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car008.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car008Red": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car008-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car020": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car020.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Car020Red": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car020-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "WagonGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#CD853F'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Wagon": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/wagon.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "PowerGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#94ba51'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Power": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/power.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "PowerOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/power_offline.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "RelayGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#4b91ff'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Relay": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/relay.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "RelayOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/relay_offline.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SubControlGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#5e6679'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "SubControl": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sub_control.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SubControlOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sub_control_offline.png',
            scale: 0.42
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "PersonGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#26c7dd'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Person": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/person.png',
            scale: 0.5,
            anchor: [0.5, 1]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -48
        })
    }),
    "Person_1": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/person_1.png',
            scale: 0.5,
            anchor: [0.5, 1]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -48
        })
    }),
    "Person_2": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/person_2.png',
            scale: 0.5,
            anchor: [0.5, 1]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -48
        })
    }),
    "Person01": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/person01.png',
            scale: 0.5,
            anchor: [0.5, 1]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -48
        })
    }),
    "Person01_1": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/person01_1.png',
            scale: 0.5,
            anchor: [0.5, 1]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -48
        })
    }),
    "Person01_2": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/person01_2.png',
            scale: 0.5,
            anchor: [0.5, 1]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -48
        })
    }),
    "TrafficLightGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#34df26'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "GreenLight0": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/greenlight_0.png',
            scale: 0.08,
            anchor: [0.5, 0.5]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "YellowLight0": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/yellowlight_0.png',
            scale: 0.08,
            anchor: [0.5, 0.5]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "RedLight0": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/redlight_0.png',
            scale: 0.08,
            anchor: [0.5, 0.5]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "TrafficLightOffline0": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/trafficlight_offline_0.png',
            scale: 0.08,
            anchor: [0.5, 0.5]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "GreenLight1": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/greenlight_1.png',
            scale: 0.08,
            anchor: [0.5, 0.5]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "YellowLight1": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/yellowlight_1.png',
            scale: 0.08,
            anchor: [0.5, 0.5]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "RedLight1": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/redlight_1.png',
            scale: 0.08,
            anchor: [0.5, 0.5]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "TrafficLightOffline1": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/trafficlight_offline_1.png',
            scale: 0.08,
            anchor: [0.5, 0.5]
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "LedGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#52b131'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Led": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/led.png',
            scale: 0.15
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "LedOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/led_offline.png',
            scale: 0.15
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -24
        })
    }),
    "Monorail": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/monorail.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Locomotive": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/locomotive.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "MonorailOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/monorail_offline.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "MonorailGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#CB7B65'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "RailGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#CB7B65'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "CarPark": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/car_park.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "CarParkGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#3989C3'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Switch": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/switch.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SwitchOffline": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/switch_offline.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SwitchStraight": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/switch_straight.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SwitchStraightLock": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/switch_straight_lock.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SwitchSwerve": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/switch_swerve.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SwitchSwerveLock": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/switch_swerve_lock.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SwitchWarn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/switch_warn.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SwitchGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#67850E'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Damper": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/damper.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "DamperGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#886E67'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Ramp": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/ramp.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "RampGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#259994'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "MonitorStation": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/monitorStation.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "MonitorStationGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#4B91FF'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "BeamTube": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/beamTube.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BeamTubeWarn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/beamTube-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "BeamTubeGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#597DB4'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Sensor0001": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0001.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0001Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0001-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0002": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0002.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0002Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0002-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0003": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0003.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0003Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0003-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0004": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0004.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0004Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0004-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0012": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0012.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0012Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0012-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0013": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0013.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0013Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0013-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0014": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0014.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0014Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0014-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1008": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1008.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1008Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1008-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1002": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1002.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1002Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1002-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1003": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1003.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1003Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1003-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0011": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0011.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0011Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0011-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1004": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1004.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1004Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1004-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1009": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1009.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1009Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1009-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1011": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1011.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1011Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1011-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1012": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1012.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor1012Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor1012-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0005": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0005.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0005Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0005-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0049": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0049.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0049Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0049-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0022": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0022.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0022Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0022-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0025": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0025.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0025Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0025-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0023": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0023.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0023Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0023-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0038": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0038.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0038Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0038-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0040": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0040.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "Sensor0040Warn": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/sensor0040-red.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "SensorGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#06BEB3'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Risk": new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#26c7dd'
        }),
        fill: new ol.style.Fill({
            color: '#26c7dd88'
        })
    }),
    "VehicleArea": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/vehicle_area.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "VehicleAreaGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#9257ff'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "IotDeviceGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#06BEB3'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "IotDevice": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/co_sensor.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "MovableDeviceGroup":new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#06BEB3'
            })}),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "VehicleVeGroup": new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#7993ba'
            })
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "VehicleVe101": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/managePlatform/101.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "VehicleVe102": new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/managePlatform/102.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    }),
    "FireAlarmGroup":new ol.style.Style({
        image: new ol.style.Circle({
            radius: 16,
            stroke: new ol.style.Stroke({
                color: '#eee'
            }),
            fill: new ol.style.Fill({
                color: '#ff0000'
            })}),
        text: new ol.style.Text({
            font: "20px Calibri,sans-serif bold",
            fill: new ol.style.Fill({
                color: '#eee'
            })
        })
    }),
    "Null": new ol.style.Style(),
};

const car_img_prefix = ["a_", "b_", "c_", "d_", "e_", "warn_"];
const car_img_index = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16",
    "17", "18", "19"];

for (let p = 0; p < car_img_prefix.length; p++) {
    for (let i = 0; i < car_img_index.length; i++) {
        let imgName = car_img_prefix[p] + car_img_index[i];
        let key = "Car" + imgName;
        _styles[key] = new ol.style.Style({
            image: new ol.style.Icon({
                src: 'portal/gis2d/images/car/' + imgName +'.png',
                scale: 0.1
            }),
            text: new ol.style.Text({
                font: config.fontStyle,
                fill: new ol.style.Fill({
                    color: config.titleColor
                }),
                offsetY: -22
            })
        })
    }
}

const movableDevice_img_name = ["UNKNOWN", "BPQ", "BYQ", "CMJ", "CFB", "DJ", "FT", "GBKG", "GBJHJ", "GBJ", "JJJ", "SXT",
    "SSJ", "SB", "TFJ", "YFJ", "ZZJ"];

for (let i = 0; i < movableDevice_img_name.length; i++) {
    let key = "MovableDevice" + movableDevice_img_name[i];
    let keyWarn = key + "Warn"
    _styles[key] = new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/movableDevice/' + movableDevice_img_name[i] +'.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    });
    _styles[keyWarn] = new ol.style.Style({
        image: new ol.style.Icon({
            src: 'portal/gis2d/images/movableDevice/' + movableDevice_img_name[i] +'_warn.png',
            scale: 0.1
        }),
        text: new ol.style.Text({
            font: config.fontStyle,
            fill: new ol.style.Fill({
                color: config.titleColor
            }),
            offsetY: -22
        })
    });
}

// _styles[]
/*let carTypeDic = {
      "001":"08",
      "002":"07",
      "003":"06",
      "004":"05",
      "005":"04",
      "006":"03",
      "007":"02",
      "008":"01"
  };*/

_styleFunc = {
    "Station": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["StationGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Station"];
            if (items[0].get("state") === 1) {
                style = _styles["StationOffline"];
            }
            if (labelFlags && labelFlags["Station"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.6);
                style.getText().setOffsetY(-27);
            } else {
                style.getImage().setScale(0.4);
                style.getText().setOffsetY(-18);
            }
            return style;
        }
    },
    "Person": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["PersonGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let tagName = "Person";
            if (items[0]) {
                let subType = items[0].get("subType");
                let lampFlag = items[0].get("minelamp");
                if (subType == "1") {//leader
                    tagName = tagName + "01";
                }
                if (lampFlag === 1 || lampFlag === 2) {//设备卡
                    tagName = tagName + "_" + lampFlag
                }
            }
            let style = _styles[tagName];

            if (labelFlags && labelFlags["Person"] == "1") {
                !style || style.getText().setText(items[0].get("name"));
            } else {
                !style || style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.7);
                style.getText().setOffsetY(-68);
            } else {
                style.getImage().setScale(0.5);
                style.getText().setOffsetY(-48);
            }
            return style;
        }
    },
    "Camera": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["CameraGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Camera"];
            let state = items[0].get("state");
            if (state === 1) {
                style = _styles["CameraOffline"];
            }
            if (labelFlags && labelFlags["Camera"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.7);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.5);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    "Broadcast": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["BroadcastGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let subType = items[0].get("subType");
            let broadcastType = "Broadcast";
            if (subType) {
                broadcastType += subType;
            }
            let style = _styles[broadcastType];

            let state = items[0].get("state");
            if (state === 1) {
                style = _styles[broadcastType + "Offline"];
            }
            if (labelFlags && labelFlags["Broadcast"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.7);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.5);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    "Car": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["CarGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let subType = items[0].get("subType");
            //设置默认图例
            if (null == subType) {
                subType = 'c_19';
            }
            //告警变红
            let state = items[0].get("state");
            if (2 == state) {
                subType = subType.replace(/[a-e]/, 'warn');
            }
            let imageType = "Car" + subType;
            let style = _styles[imageType];
			if (undefined == style) {
                  style = _styles["Cara_11"];
            }
            if (labelFlags && labelFlags["Car"] == "1") {
                !style || style.getText().setText(items[0].get("name"));
            } else {
                !style || style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            let rad = items[0].get("rad");
            if (!isNaN(rad)) {
                style.getImage().setRotation(rad);
            } else {
                style.getImage().setRotation(0);
            }
            return style;
        }
    },
    "Rail": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["RailGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let subType = items[0].get("subType");
            let style = null;
            let state = items[0].get("state");
            switch (subType) {
                case "101":
                    state = items[0].get("state");
                    if (state === 1) {
                        style = _styles["MonorailOffline"];
                    } else {
                        style = _styles["Monorail"];
                    }
                    break;
                case "102":
                    style = _styles["Locomotive"];
                    break;
                case "103":
                    break;
                case "104":
                    break;
                default:
                    style = _styles["Monorail"];
            }

            if (labelFlags && labelFlags["Rail"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                if (subType === "101") {
                    style.getImage().setScale(0.25);
                    style.getText().setOffsetY(-30);
                } else {
                    style.getImage().setScale(0.25);
                    style.getText().setOffsetY(-34);
                }
            } else {
                if (subType === "101") {
                    style.getImage().setScale(0.18);
                    style.getText().setOffsetY(-22);
                } else {
                    style.getImage().setScale(0.15);
                    style.getText().setOffsetY(-24);
                }
            }
            return style;
        }
    },
    "Telphone": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["TelphoneGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Telphone"];
            let state = items[0].get("state");
            if (state === 1) {
                style = _styles["TelphoneOffline"];
            }
            if (labelFlags && labelFlags["Telphone"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.7);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.5);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    "TrafficLight": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["TrafficLightGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let state = items[0].get("state");
            let lightState = items[0].get("lightState");
            let subType = items[0].get("subType");
            let styleName = "GreenLight" + subType;

            if (state === 1) {
                styleName = "TrafficLightOffline" + subType;
                items[0].set("visible", true, true);
            } else {
                if (lightState) {
                    switch (lightState) {
                        case 4:
                            styleName = "RedLight" + subType;
                            items[0].set("visible", true, true);
                            break;
                        case 7:
                            styleName = "RedLight" + subType;
                            items[0].set("visible", !items[0].get("visible"), true);
                            break;
                        case 5:
                            styleName = "YellowLight" + subType;
                            items[0].set("visible", true, true);
                            break;
                        case 8:
                            styleName = "YellowLight" + subType;
                            items[0].set("visible", !items[0].get("visible"), true);
                            break;
                        case 6:
                            styleName = "GreenLight" + subType;
                            items[0].set("visible", true, true);
                            break;
                        case 9:
                            styleName = "GreenLight" + subType;
                            items[0].set("visible", !items[0].get("visible"), true);
                            break;
                    }
                } else {
                    styleName = "YellowLight" + subType;
                    items[0].set("visible", false, true);
                }
            }

            let style = _styles[styleName];
            if (labelFlags && labelFlags["TrafficLight"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.112);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.08);
                style.getText().setOffsetY(-24);
            }
            style.getImage().setOpacity((items[0].get("visible") ? 1 : 0.2));

            let rad = items[0].get("rad");
            rad = rad == undefined ? 0 : rad;
            style.getImage().setRotation(rad);
            return style;
        }
    },
    "Led": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["LedGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Led"];
            let state = items[0].get("state");
            if (state === 1) {
                style = _styles["LedOffline"];
            }
            if (labelFlags && labelFlags["Led"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.21);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.15);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    "Wagon": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["WagonGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Wagon"];
            if (labelFlags && labelFlags["Wagon"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.21);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.15);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    "Power": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["PowerGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Power"];
            let state = items[0].get("state");
            if (state === 1) {
                style = _styles["PowerOffline"];
            }
            if (labelFlags && labelFlags["Power"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.59);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.42);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    "Relay": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["RelayGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Relay"];
            let state = items[0].get("state");
            if (state === 1) {
                style = _styles["RelayOffline"];
            }
            if (labelFlags && labelFlags["Relay"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.59);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.42);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    "SubControl": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["SubControlGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["SubControl"];
            let state = items[0].get("state");
            if (state === 1) {
                style = _styles["SubControlOffline"];
            }
            if (labelFlags && labelFlags["SubControl"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.59);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.42);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    "Monorail": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["MonorailGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Monorail"];
            let state = items[0].get("state");
            if (state === 1) {
                style = _styles["MonorailOffline"];
            }
            if (labelFlags && labelFlags["Monorail"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.25);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.18);
                style.getText().setOffsetY(-22);
            }
            return style;
        }
    },
    "Switch": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["SwitchGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let switchState = items[0].get("switchState");
            let state = items[0].get("state");
            let styleName = "Switch";
            let style;
            if (state === 1) {
                style = _styles["SwitchOffline"];
            } else {
                switch (switchState) {
                    case 10:
                        styleName = "SwitchStraight";
                        break;
                    case 11:
                        styleName = "SwitchStraightLock";
                        break;
                    case 20:
                        styleName = "SwitchSwerve";
                        break;
                    case 21:
                        styleName = "SwitchSwerveLock";
                        break;
                    case 3:
                        styleName = "SwitchWarn";
                        break;
                }
                style = _styles[styleName];
            }

            if (labelFlags && labelFlags["Switch"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            return style;
        }
    },
    "CarPark": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["CarParkGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["CarPark"];
            if (labelFlags && labelFlags["CarPark"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            return style;
        }
    },
    "Damper": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["DamperGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Damper"];
            if (labelFlags && labelFlags["Damper"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            return style;
        }
    },
    "Ramp": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["RampGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Ramp"];
            if (labelFlags && labelFlags["Ramp"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            return style;
        }
    },
    "IotDevice442": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["SensorGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let subType = items[0].get("subType");
            let imageType = "Sensor";

            switch (subType) {
                case "0001": //环境瓦斯
                case "0002": //风速
                case "0003": //环境温度
                case "0004": //一氧化碳
                case "0012": //氧气
                case "0013": //二氧化碳
                case "0014": //粉尘
                case "1008": //烟雾
                case "1002": //风门
                case "1003": //风筒状态
                case "0011": //高低浓瓦斯
                case "1004": //设备开停
                case "1009": //断电器
                case "1011": //馈电器
                case "1012": //声光报警器
                case "0005": //风压
                case "0049": //液位
                case "0022": //管道瓦斯
                case "0025": //管道压力
                case "0023": //管道温度
                case "0038": //管道一氧化碳
                case "0040": //管道流量
                    imageType += subType;
            }
            let state = items[0].get("state");
            //告警变红
            if (0 != state) {
                imageType += "Warn";
            }

            let style = _styles[imageType];
            if (labelFlags && labelFlags["IotDevice442"] == "1") {
                !style || style.getText().setText(items[0].get("name"));
            } else {
                !style || style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            let rad = items[0].get("rad");
            if (!isNaN(rad)) {
                style.getImage().setRotation(rad);
            } else {
                style.getImage().setRotation(0);
            }
            return style;
        }
    },
    "MonitorStation": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["MonitorStationGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let imageType = "MonitorStation";
            let state = items[0].get("state");
            //告警变红
            if (0 != state) {
                imageType += "Warn";
            }
            let style = _styles[imageType];
            if (labelFlags && labelFlags["MonitorStation"] == "1") {
                !style || style.getText().setText(items[0].get("name"));
            } else {
                !style || style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            let rad = items[0].get("rad");
            if (!isNaN(rad)) {
                style.getImage().setRotation(rad);
            } else {
                style.getImage().setRotation(0);
            }
            return style;
        }
    },
    "BeamTube": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["BeamTubeGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let imageType = "BeamTube";
            let state = items[0].get("state");
            //告警变红
            if (0 != state) {
                imageType += "Warn";
            }
            let style = _styles[imageType];
            if (labelFlags && labelFlags["BeamTube"] == "1") {
                !style || style.getText().setText(items[0].get("name"));
            } else {
                !style || style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            let rad = items[0].get("rad");
            if (!isNaN(rad)) {
                style.getImage().setRotation(rad);
            } else {
                style.getImage().setRotation(0);
            }
            return style;
        }
    },
    "Risk": function (feature, resolution) {
        let style = _styles["Risk"];
        let colorType = feature.get('colorType');
        if (riskModel == 1) {
            colorType = 0;
        } else if (riskModel == 2 && colorType == 0) {
            return _styles["Null"];
        }
        style.getStroke().setColor(colorTypes[colorType]);
        style.getFill().setColor(colorTypes[colorType] + "88");
        return style;
    },
    "VehicleArea": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["VehicleAreaGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["VehicleArea"];
            if (labelFlags && labelFlags["VehicleArea"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.12);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-24);
            }
            return style;
        }
    },
    //442号文的人没有卡类型数据
    "Person442": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["PersonGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let tagName = "Person";
            if (items[0]) {
                let subType = items[0].get("subType");
                if (subType == "1") {//leader
                    tagName = tagName + "01";
                }
            }
            let style = _styles[tagName];

            if (labelFlags && labelFlags["Person442"] == "1") {
                !style || style.getText().setText(items[0].get("name"));
            } else {
                !style || style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.7);
                style.getText().setOffsetY(-68);
            } else {
                style.getImage().setScale(0.5);
                style.getText().setOffsetY(-48);
            }
            return style;
        }
    },
    "Station442": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["StationGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Station"];
            if (items[0].get("state") === 1) {
                style = _styles["StationOffline"];
            }
            if (labelFlags && labelFlags["Station442"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.6);
                style.getText().setOffsetY(-27);
            } else {
                style.getImage().setScale(0.4);
                style.getText().setOffsetY(-18);
            }
            return style;
        }
    },
    "IotDevice": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["IotDeviceGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["IotDevice"];
            if (labelFlags && labelFlags["IotDevice"] == "1") {
                !style || style.getText().setText(items[0].get("name"));
            } else {
                !style || style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            let rad = items[0].get("rad");
            if (!isNaN(rad)) {
                style.getImage().setRotation(rad);
            } else {
                style.getImage().setRotation(0);
            }
            return style;
        }
    },
    "MovableDevice" : function(feature, resolution) {
	      let items = feature.get("features");
	      if (items.length > 1) {
	          let style = _styles["MovableDeviceGroup"];
	          style.getText().setText(items.length.toString());
	          return style;
	      } else {
	          let subType = items[0].get("subType");
	          if (subType == null || subType == "") {
	              subType = "UNKNOWN";
	          }
	          let styleName = "MovableDevice" + subType;
	          let state = items[0].get("state");
	          //告警变红
	          if (1 == state) {
	              styleName += "Warn";
	          }
	
	          let style = _styles[styleName]
	          if(labelFlags && labelFlags["MovableDevice"] == "1") {
	              !style || style.getText().setText(items[0].get("name"));
	          } else {
	              !style || style.getText().setText("");
	          }
	          if (!!items[0].get("highlight")) {
	              style.getImage().setScale(0.14);
	              style.getText().setOffsetY(-30);
	          } else {
	              style.getImage().setScale(0.1);
	              style.getText().setOffsetY(-22);
	          }
	          let rad = items[0].get("rad");
	          if (!isNaN(rad)) {
	              style.getImage().setRotation(rad);
	          } else {
	              style.getImage().setRotation(0);
	          }
	          return style;
	      }
      },    
    "TrafficLightVe": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["TrafficLightGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let state = items[0].get("state");
            let styleName = "GreenLight0";

            if (state) {
                switch (state) {
                    case 0:
                        styleName = "TrafficLightOffline0";
                        items[0].set("visible", true, true);
                        break;
                    case 4:
                        styleName = "RedLight0";
                        items[0].set("visible", true, true);
                        break;
                    case 7:
                        styleName = "RedLight0";
                        items[0].set("visible", !items[0].get("visible"), true);
                        break;
                    case 5:
                        styleName = "YellowLight0";
                        items[0].set("visible", true, true);
                        break;
                    case 8:
                        styleName = "YellowLight0";
                        items[0].set("visible", !items[0].get("visible"), true);
                        break;
                    case 6:
                        styleName = "GreenLight0";
                        items[0].set("visible", true, true);
                        break;
                    case 9:
                        styleName = "GreenLight0";
                        items[0].set("visible", !items[0].get("visible"), true);
                        break;
                }
            } else {
                styleName = "YellowLight0";
                items[0].set("visible", false, true);
            }

            let style = _styles[styleName];
            if (labelFlags && labelFlags["TrafficLightVe"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.112);
                style.getText().setOffsetY(-36);
            } else {
                style.getImage().setScale(0.08);
                style.getText().setOffsetY(-24);
            }
            style.getImage().setOpacity((items[0].get("visible") ? 1 : 0.2));
            return style;
        }
    },
    "StationVe": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["StationGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let style = _styles["Station"];
            if (items[0].get("state") === 1) {
                style = _styles["StationOffline"];
            }
            if (labelFlags && labelFlags["StationVe"] == "1") {
                style.getText().setText(items[0].get("name"));
            } else {
                style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.6);
                style.getText().setOffsetY(-27);
            } else {
                style.getImage().setScale(0.4);
                style.getText().setOffsetY(-18);
            }
            return style;
        }
    },
    "VehicleVe": function (feature, resolution) {
        let items = feature.get("features");
        if (items.length > 1) {
            let style = _styles["VehicleVeGroup"];
            style.getText().setText(items.length.toString());
            return style;
        } else {
            let subType = items[0].get("subType");
            let imageType = "VehicleVe" + subType;
            let style = _styles[imageType];
            if (labelFlags && labelFlags["VehicleVe"] == "1") {
                !style || style.getText().setText(items[0].get("name"));
            } else {
                !style || style.getText().setText("");
            }
            if (!!items[0].get("highlight")) {
                style.getImage().setScale(0.14);
                style.getText().setOffsetY(-30);
            } else {
                style.getImage().setScale(0.1);
                style.getText().setOffsetY(-22);
            }
            let rad = items[0].get("rad");
            if (!isNaN(rad)) {
                style.getImage().setRotation(rad);
            } else {
                style.getImage().setRotation(0);
            }
            return style;
        }
    },
    "FireAlarm" : function(feature, resolution) {
          let items = feature.get("features");
          if (items.length > 1) {
              let style = _styles["FireAlarmGroup"];
              style.getText().setText(items.length.toString());
              return style;
          } else {
              return gif.style
          }
     },
};

/**
 * 水波纹效果
 */
function WaterWave(config) {
    this.configs = config || {};
    this.TWO_PI = 2 * Math.PI;
    this.radius = this.configs.radius || 45;
    this.centerX = this.radius;
    this.centerY = this.radius;
    this.speed = this.configs.speed || 0.5;
    this.minRadius = this.radius;
    this.rgb = this.rgb || "219, 255, 220";
    this.maxRadius = this.radius * 4;
    this.tripleRadius = this.radius * 3;
    this.image = new Image();
    /**外部的图片数据 */
    this.image.src = imageData;
    this.waveRadius = [this.radius, this.radius * 2, this.radius * 3];
    /**中心标注图片的尺寸 */
    this.anchorSize = 43;

    /**
     * 绘制入口
     * @param {Canvas} ctx
     * @param {半径} size
     */
    this.draw = function (ctx) {
        ctx.save();
        //设置最大裁剪区
        ctx.beginPath();
        ctx.arc(this.centerX, this.centerY, this.tripleRadius, 0, this.TWO_PI, false);
        ctx.clip();
        //绘制水波纹
        for (let i = 0; i < this.waveRadius.length; i++) {
            this.drawWave(ctx, i);
        }
        //绘制中心锚点图片
        ctx.drawImage(this.image, this.centerX - this.anchorSize, this.centerY - this.anchorSize);
        ctx.restore();
    };

    //绘制对应层次水波纹
    this.drawWave = function (ctx, index) {
        let gradient = ctx.createRadialGradient(this.centerX, this.centerY, this.waveRadius[index] * 0.8, this.centerX, this.centerY, this.waveRadius[index]);
        let alpha = ((this.tripleRadius - Math.min(this.waveRadius[index], this.tripleRadius)) * 1.0 / this.tripleRadius) * 0.3;
        gradient.addColorStop(0, 'rgba(' + this.rgb + ', 0)');
        gradient.addColorStop(0.8, 'rgba(' + this.rgb + ', ' + (0.01 + alpha) + ')');
        gradient.addColorStop(1, 'rgba(' + this.rgb + ', ' + (0.06 + alpha) + ')');
        ctx.beginPath();

        ctx.arc(this.centerX, this.centerY, Math.min(this.waveRadius[index], this.maxRadius), 0, this.TWO_PI, false);
        ctx.fillStyle = gradient;
        ctx.fill();
        this.waveRadius[index] += this.speed;

        if (this.waveRadius[index] > this.maxRadius) {
            this.waveRadius[index] = this.minRadius;
        }
    };

    /**
     * 更新中心坐标
     * @param {中心坐标} position
     */
    this.updateWaterWave = function (position) {
        this.centerX = position[0];
        this.centerY = position[1];
    }

}

/**
 * 雷达效果
 */
function Radar() {

    this.TWO_PI = 2 * Math.PI;
    this.radius = 50;
    this.centerX = this.radius;
    this.centerY = this.radius;
    this.sweepAngle = 270;
    this.sweepSpeed = 1;
    this.rings = 4;
    this.perRad = Math.PI / 180;

    /**
     * 绘制渐变背景
     * @param {canvas} ctx
     */
    this.drawBackgroud = function (ctx) {
        let gradient = ctx.createRadialGradient(this.centerX, this.centerY, this.radius * 0.82, this.centerX, this.centerY, this.radius);
        gradient.addColorStop(0, 'rgba(14,150,109, 0.1)');
        gradient.addColorStop(0.5, 'rgba(14,150,109, 0.2)');
        gradient.addColorStop(0.96, 'rgba(14,150,144, 0.8)');
        gradient.addColorStop(1, 'rgba(14,220,220, 1)');
        ctx.beginPath();
        ctx.arc(this.centerX, this.centerY, (this.radius), 0, this.TWO_PI, false);
        ctx.fillStyle = gradient;
        ctx.fill();
    };

    /**
     * 绘制同心圆环
     * @param {Canvas} ctx
     */
    this.renderRings = function (ctx) {
        for (let i = 0; i < this.rings - 1; i++) {
            ctx.beginPath();
            ctx.arc(this.centerX, this.centerY, ((this.radius) / this.rings) * (i + 1), 0, this.TWO_PI, false);
            ctx.strokeStyle = 'rgba(0,255,255, ' + (0.1 + (i / 8.0)) + ')';
            ctx.lineWidth = 1;
            ctx.stroke();
        }
    };

    /**
     * 绘制十字坐标系
     * @param {Canvas} ctx
     */
    this.renderGrid = function (ctx) {
        ctx.beginPath();
        ctx.moveTo(this.centerX - this.radius, this.centerY);
        ctx.lineTo(this.centerX + this.radius, this.centerY);
        ctx.moveTo(this.centerX, this.centerY - this.radius);
        ctx.lineTo(this.centerX, this.centerY + this.radius);
        ctx.strokeStyle = 'rgba(14,106,108, 0.4 )';
        ctx.stroke();
    };

    /**
     * 根据角度计算圆弧上的坐标点
     * @param {半径} radius
     * @param {角度} deg
     */
    this.toPoint = function (radius, deg) {
        return {
            x: radius * Math.cos(this.perRad * deg),
            y: radius * Math.sin(this.perRad * deg),
        };
    };

    /**
     * 绘制带渐变尾巴的平扫线
     * @param {canvas} ctx
     */
    this.renderSweep = function (ctx) {
        ctx.save();
        ctx.translate(this.centerX, this.centerY);
        let endEngle = this.sweepAngle % 360;
        let size = 16;
        let opacity = 1;
        for (let i = 0; i < size; i++) {
            let deg1 = (endEngle - i - 1);
            let deg2 = (endEngle - i);

            let point1 = this.toPoint(this.radius, deg1);
            let point2 = this.toPoint(this.radius, deg2);

            if (i == 0) {
                opacity = 1;
            } else {
                opacity = 1 - (i / size) - 0.3;
            }
            ctx.beginPath();
            ctx.fillStyle = 'rgba( 0,255,255, ' + opacity + ')';
            ctx.moveTo(0, 0);
            ctx.lineTo(point1.x, point1.y);
            ctx.lineTo(point2.x, point2.y);
            ctx.fill();
        }
        ctx.restore();
    };

    /**
     * 绘制入口
     * @param {Canvas} ctx
     * @param {半径} size
     */
    this.draw = function (ctx, size) {
        this.radius = size;
        this.drawBackgroud(ctx);
        this.renderRings(ctx);
        this.renderGrid(ctx);
        this.renderSweep(ctx);
        // ctx.globalCompositeOperation = 'lighter';
        this.sweepAngle += this.sweepSpeed;
    };

    /**
     * 更新雷达中心坐标
     * @param {中心坐标} position
     */
    this.updateRadar = function (position) {
        this.centerX = position[0];
        this.centerY = position[1];
    };

    this.getCenter = function () {
        return [this.centerX, this.centerY];
    }
}