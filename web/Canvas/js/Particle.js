class Particle {
    constructor() {
        this.r = rand(2, 6);
        this.x = rand(0 + this.r, canvas.width - this.r);
        this.y = rand(0 + this.r, canvas.height - this.r);
        this.opacity = Math.random();
        this.op = 1;

    }
  
    draw() {
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2);
      let color = ctx.createRadialGradient(this.x, this.y, this.r * 0.7, this.x, this.y, this.r);
      color.addColorStop(0.2, `rgba(166, 255, 240,${this.op})`);
      color.addColorStop(1, `rgba(255, 255, 255,${this.op})`);
      ctx.shadowColor = "#A6FFF0";
      ctx.shadowBlur = this.r * 1.6;
      ctx.fillStyle = color;
      ctx.globalAlpha = this.opacity;
      ctx.closePath();
      ctx.fill();
    }
  
    move() {
      const center = { x: canvas.width / 2, y: 80 }, speed = 0.01;
      let distance = Math.sqrt(Math.pow(center.x - this.x, 2) + Math.pow(center.y - this.y, 2));
      if (distance < 10) {
        this.x = rand(0 + this.r, canvas.width - this.r);
        this.y = rand(0 + this.r, canvas.height - this.r);
      }
      this.x += (center.x - this.x) * speed;
      this.y += (center.y - this.y) * speed;
    }
  }